Dispatcher Cookbook
=================
This cookbook install the dispatcher module for AEM. 

Requirements
------------
depends "apache2", ">= 2.2.0"

Attributes
----------
['dispatcher']['renderer']['port'] = "4503"

['dispatcher']['renderer']['hostname'] = "localhost"

['dispatcher']['landing_page'] = "/content"

['dispatcher']['installation_dir'] = "/data/dispatcher"

['dispatcher']['config_file_path'] = "dispatcher.any"

['dispatcher']['log_level'] = "1"

['dispatcher']['statfileslevel'] = "1"

['dispatcher']['srv_acc'] = "dispatcher_srv"

Usage
-----
#### dispatcher::default
Setup apache with dispatcher module.

#### dispatcher::ssl
Setup apache with SSL and dispatcher module.

#### dispatcher::basic_auth
Setup apache with dispatcher module and basic authentication with a blank /etc/apache2/htpasswd. 

e.g.
Just include `dispatcher` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[dispatcher::ssl]"
  ]
}
```