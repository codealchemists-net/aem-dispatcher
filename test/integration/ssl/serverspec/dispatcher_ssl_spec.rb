
require 'serverspec'

set :backend, :exec

case os[:family]
  when 'debian', 'ubuntu'
    apache2 = 'apache2'
  when 'rhel', 'redhat'
    apache2 = 'httpd'
end

describe port(80) do
  it { should be_listening }
end

describe port(443) do
  it { should be_listening }
end

describe command('openssl s_client -connect localhost:443') do
  its(:stdout) { should contain('akqa.technology')}
end

describe service(apache2) do
  it { should be_enabled }
  it { should be_running }
end