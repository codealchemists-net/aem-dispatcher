
require 'serverspec'

set :backend, :exec

case os[:family]
  when 'debian', 'ubuntu'
    apache2 = 'apache2'
  when 'rhel', 'redhat'
    apache2 = 'httpd'
end

describe port(80) do
  it { should be_listening }
end

describe service(apache2) do
  it { should be_enabled }
  it { should be_running }
end

describe command('curl -L http://localhost/') do
  its(:stdout) { should contain('401') }
end