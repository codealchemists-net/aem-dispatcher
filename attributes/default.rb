default['dispatcher']['renderer']['port'] = "4503"
default['dispatcher']['renderer']['hostname'] = "localhost"
default['dispatcher']['landing_page'] = "/content/geometrixx/en.html"
default['dispatcher']['installation_dir'] = "/data/dispatcher"

default['dispatcher']['config_file_path'] = "dispatcher.any"
default['dispatcher']['log_level'] = "1"
default['dispatcher']['statfileslevel'] = "1"

default['dispatcher']['srv_acc'] = "dispatcher_srv"

default['mod_pagespeed']['package_linke'] = "https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_x86_64.rpm"

