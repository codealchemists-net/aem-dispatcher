if node["platform"] == "suse"
  node.override['apache']['binary'] = '/usr/sbin/httpd'

  execute 'Add SuSE Apache24 repos' do
    command 'zypper -q --gpg-auto-import-keys ar http://download.opensuse.org/repositories/Apache/SLE_11_SP3/Apache.repo'
    not_if { File.exists?('/etc/zypp/repos.d/Apache.repo') }
  end
  execute 'Add SuSE Lua repo' do
    command 'zypper -q --gpg-auto-import-keys ar http://download.opensuse.org/repositories/home:/kkeil:/Testing/SLE_11_SP2/home:kkeil:Testing.repo'
    not_if { File.exists?('/etc/zypp/repos.d/home_kkeil_Testing.repo') }
  end

  execute 'Install apache2' do
    command 'zypper -q --gpg-auto-import-keys --non-interactive install libapr1-1.5.2 && \
    zypper -q --gpg-auto-import-keys --non-interactive install libapr-util1-1.5.4 && \
    zypper -q --gpg-auto-import-keys --non-interactive install apache2-utils-2.4.16 && \
    zypper -q --gpg-auto-import-keys --non-interactive install liblua5_1-5.1.4 && \
    zypper -q --gpg-auto-import-keys --non-interactive install apache2-2.4.16 && \
    zypper -q --gpg-auto-import-keys --non-interactive install apache2-prefork-2.4.16'
  end 

  execute 'Create sym-links to deprecated-scripts' do
     command 'ln -s /usr/share/apache2/deprecated-scripts/* /usr/share/apache2/.'
     not_if { File.exists?('/usr/share/apache2/get_module_list') }
  end

  template "/etc/init.d/apache2" do
    source "suse/apache2.erb"
    mode "0755"
    action :create
  end
end

include_recipe "apache2"

arch = node['kernel']['machine'] =~ /x86_64/ ? "amd64" : "i386"

# Add the necessary modules to apache

%w{ rewrite expires ssl }.each do |mod|
  include_recipe "apache2::mod_#{mod}"
end

["rewrite","expires","ssl"].each do |mod_name|
  apache_module mod_name
end

#apache_module "socache_shmcb" if node['platform_family'] == 'debian'


# Add the dispatcher site
web_app "dispatcher" do
  server_aliases [node['fqdn'], "localhost"]
  application_name "dispatcher"
  docroot node['dispatcher']['installation_dir']
  server_name node['hostname']
  if node['apache']['version'] == '2.4'
    template "dispatcher.2.4.conf.erb"
  else
    template "dispatcher.conf.erb"
  end
  landing_page node['dispatcher']['landing_page']
end

# Set up the docroot directory for the dispatcher virtual host
directory node['dispatcher']['installation_dir'] do
	owner node['apache']['user']
	group node['apache']['group']
	mode "0755"
	action :create
  recursive true
end


#Set up the dispatcher config
template "#{node['apache']['dir']}/dispatcher.any" do
  source "dispatcher.any.erb"
  mode "0644"
  variables(
    :renderer_port => "#{node['dispatcher']['renderer']['port']}",
    :renderer_hostname => "#{node['dispatcher']['renderer']['hostname']}",
    :serveStaleOnError => "1",
    :statfileslevel => "1",
    :docroot => "#{node['dispatcher']['installation_dir']}"
  )
  action :create_if_missing
end

# Copy other dispatcher dependency configurations from dispatcher folder to client’s /etc/httpd/dispatcher folder
remote_directory "#{node['apache']['dir']}/dispatcher" do
  source "dispatcher"
  files_owner "root"
  files_group "root"
  files_mode 00644
  owner "root"
  group "root"
  mode 0755
end

template "#{node['apache']['dir']}/dispatcher/render.any" do
  source "render.any.erb"
  mode "0644"
  variables(
    :renderer_port => "#{node['dispatcher']['renderer']['port']}",
    :renderer_hostname => "#{node['dispatcher']['renderer']['hostname']}"
  )
  action :create_if_missing
end

# Copy dispatcher module into place
cookbook_file "#{node['apache']['libexec_dir']}/mod_dispatcher.so" do
	owner "root"
	group "root"
	mode "0644"

  if node['apache']['version'] == '2.4'
    case arch
      when "amd64"
        source "dispatcher-apache2.4-4.1.12-x86-64.so"
      when "i386"
        source "dispatcher-apache2.4-4.1.12-i686.so"
    end
  else
    case arch
      when "amd64"
        source "dispatcher-apache2.2-4.1.12-x86-64.so"
      when "i386"
        source "dispatcher-apache2.2-4.1.12-i686.so"
    end
  end
end

# Enable the dispatcher module
apache_module "dispatcher" do
  enable "true"
  conf true
end

# Remove the default web site
apache_site "default" do
	enable false
end

