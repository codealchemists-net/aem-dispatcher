if node['platform'] == 'suse'
  # mpm_prefork module is removed because on SLES Apache works in prefork mode after installing apache2-prefork-2.4 package.
  # unixd.load is loading by default on SLES 11 SP2
  execute 'Remove unsupported modules ' do
    command 'rm /etc/apache2/mods-enabled/mpm_prefork.load && \
    rm /etc/apache2/mods-enabled/unixd.load'
  end
  execute 'Restart apache' do
    command 'apache2ctl restart'
  end
end