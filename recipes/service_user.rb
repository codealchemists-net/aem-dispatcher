#
# Recipe:   service_user.rb
# Purpose:  automate creation of service account to manage dispatcher configs
#

user node['dispatcher']['srv_acc'] do
  action :create
  shell "/bin/bash"
end

group "Add service user to group" do
  group_name  case node["platform"]
              when "ubuntu", "debian"
                'sudo'
              when "redhat", "centos"
                'wheel'
              end
  action :modify
  members "#{node['dispatcher']['srv_acc']}"
  append true
end

template "/etc/sudoers.d/99_#{node['dispatcher']['srv_acc']}" do
  action :create
  source "sudoers.d/99_dispatcher_srv.erb"
  owner "root"
  group "root"
  mode 0440
end

directory "/home/#{node['dispatcher']['srv_acc']}/.ssh" do
  action :create
  recursive true
  mode 0700
  group node['dispatcher']['srv_acc']
  owner node['dispatcher']['srv_acc']
end

cookbook_file "authorized_keys" do
  source "ssh/authorized_keys"
  path "/home/#{node['dispatcher']['srv_acc']}/.ssh/authorized_keys"
  action :create_if_missing
  mode 0600
  group node['dispatcher']['srv_acc']
  owner node['dispatcher']['srv_acc']
end
