#
# Recipe: mod_pagespeed.rb
#

if platform_family?('rhel')
  remote_file "#{Chef::Config[:file_cache_path]}/mod-pagespeed-stable_current_x86_64.rpm" do
    source node['mod_pagespeed']['package_linke']
    mode '0644'
    action :create_if_missing
  end

  package 'mod_pagespeed' do
    source "#{Chef::Config[:file_cache_path]}/mod-pagespeed-stable_current_x86_64.rpm"
    action :install
  end

  apache_module 'pagespeed' do
    conf true
    filename "mod_pagespeed_ap24.so"
  end

  apache_conf 'pagespeed_libraries' do
    enable true
  end

  directory "#{node['apache']['dir']}/conf.d" do
    action :delete
    recursive true
  end
else
  include_recipe "apache2::mod_pagespeed"
end

